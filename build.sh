#!/bin/sh
set -e

APPID="org.electroncash.ElectronCash"

PATH_BUILD="build"
PATH_REPO="repo"
PATH_BUNDLE="ElectronCash.flatpak"

TRANSLATIONS_URL="https://crowdin.com/backend/download/project/electrum.zip"
TRANSLATIONS_PATH="translations.zip"
TRANSLATIONS_UPDATE=true
for arg in "$@";
do
	if [ "${arg}" = "--disable-download" ] || [ "${arg}" = "--disable-updates" ];
	then
		TRANSLATIONS_UPDATE=false
	fi
done

if ! [ -e "${TRANSLATIONS_PATH}" ] || ${TRANSLATIONS_UPDATE};
then
	curl -LR --time-cond "${TRANSLATIONS_PATH}" -o "${TRANSLATIONS_PATH}" "${TRANSLATIONS_URL}"
fi

flatpak-builder --ccache --force-clean --repo "${PATH_REPO}" "$@" "${PATH_BUILD}" "${APPID}.json"
flatpak build-bundle "${PATH_REPO}" "${PATH_BUNDLE}" "${APPID}"